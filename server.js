var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var calc = require('./calc.js')

app.get('/', (req, res) => {
	res.sendFile(__dirname + '/index.html');
})

io.on('connection', (socket) => {
	socket.on('calculate', (expression) => {
		calc(expression, res => {
			socket.emit('show result', res);
		})
	})
})

server.listen(3000, () => {
	console.log('Listening on *:3000');
})