var prioritet = {
	'+': '0',
	'-': '0',
	'*': '1',
	'/': '1'
}

var isValid = function(char) {
	
}

var isOperand = function(char) {
	return char == '+' ||
			char == '-' ||
			char == '*' ||
			char == '/';
}

var solve = function(operator, left, right) {
	if (operator == '+') {
		return left + right;
	}
	if (operator == '-') {
		return left - right;
	}
	if (operator == '*') {
		return left * right;
	}
	if (operator == '/') {
		return left / right;
	}
}

if (!Array.prototype.last){
    Array.prototype.last = function(){
        return this[this.length - 1];
    };
};

var calculate = (expr, callback) => {
	var rpn = [];
	var operations = [];
	var summ = 0;
	var numberOfBrackets = 0;
	expr.split(' ').forEach(token => {
		if (token[0] == '(') {
			operations.push('(');
			rpn.push(parseInt(token.slice(1)));
		} else if (token[token.length - 1] == ')') {
			rpn.push(parseInt(token.slice(0, token.length - 1)));
			while (operations.last() != '(') {
				rpn.push(operations.last());
				operations.pop();
			}
			operations.pop();
		} else if (isOperand(token)) {
			if (operations.length != 0) {
				if (prioritet[operations.last()] >= prioritet[token]) {
					rpn.push(operations.last());
					operations.pop();
				}
			}
			operations.push(token);
		} else {
			rpn.push(parseInt(token));
		}
	})
	while (operations.length != 0) {
		rpn.push(operations.last());
		operations.pop();
	}
	console.log(rpn);
	var stack = [];
	for (var i = 0; i < rpn.length; i++) {
		if (!isOperand(rpn[i])) {
			stack.push(rpn[i]);
		} else {
			var right = stack.pop();
			var left = stack.pop();
			stack.push(solve(rpn[i], left, right));
		}
	}
	callback(stack.pop());
}

module.exports = calculate;
